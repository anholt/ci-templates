# vim: set expandtab shiftwidth=2 tabstop=8 textwidth=0:

#################################################################
#                                                               #
#                    bootstrapping stage                        #
#                                                               #
#################################################################

# we need a minimalist image capable of buildah, podman, skopeo, curl,
# jq, date and test. We used to rely on `bootstrap/bootstrap.sh`, but
# a commit in runc prevented it to be compiled against musl. So we just
# end up building a regular container image from arch.
#
# To avoid rebuilding this image at every run, and to give better control,
# BOOTSTRAP_TAG needs to be set by the caller.
.bootstrap:
  image: archlinux/base:latest
  stage: bootstrapping
  before_script:
  # install buildah and podman
  - pacman -S --refresh
  - pacman -S --noconfirm buildah podman
  - |
    cat > /etc/containers/storage.conf <<EOF
    [storage]
    driver = "vfs"
    runroot = "/var/run/containers/storage"
    graphroot = "/var/lib/containers/storage"
    EOF

  # bug in podman?
  - podman info

  # login to the registry
  - podman login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

  # check if our image is already in the current registry
  - skopeo inspect docker://$CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG > /dev/null && exit 0 || true

  script:
  # https://gitlab.com/gitlab-com/support-forum/issues/4349
  - export BUILDAH_FORMAT=docker

  - buildcntr=$(buildah from fedora:29)
  - buildah run $buildcntr dnf upgrade -y
  - buildah run $buildcntr dnf install -y buildah podman skopeo jq git
    # do not store the packages database, it's pointless
  - buildah run $buildcntr dnf clean all

    # set up the working directory
  - buildah config --workingdir /app $buildcntr
  - export buildmnt=$(buildah mount $buildcntr)
  - |
    cat > $buildmnt/etc/containers/registries.conf <<EOF
    # This is a system-wide configuration file used to
    # keep track of registries for various container backends.
    # It adheres to TOML format and does not support recursive
    # lists of registries.

    # The default location for this configuration file is /etc/containers/registries.conf.

    # The only valid categories are: 'registries.search', 'registries.insecure',
    # and 'registries.block'.

    [registries.search]
    registries = ['docker.io', 'registry.fedoraproject.org', 'quay.io', 'registry.centos.org']

    # If you need to access insecure registries, add the registry's fully-qualified name.
    # An insecure registry is one that does not have a valid SSL certificate or only does HTTP.
    [registries.insecure]
    registries = []


    # If you need to block pull access from a registry, uncomment the section below
    # and add the registries fully-qualified name.
    #
    # Docker only
    [registries.block]
    registries = []
    EOF

  - |
    cat > $buildmnt/etc/containers/policy.json <<EOF
    {
        "default": [
            {
                "type": "insecureAcceptAnything"
            }
        ],
        "transports":
            {
                "docker-daemon":
                    {
                        "": [{"type":"insecureAcceptAnything"}]
                    }
            }
    }
    EOF

  - |
    cat > $buildmnt/etc/containers/storage.conf <<EOF
    # This file is is the configuration file for all tools
    # that use the containers/storage library.
    # See man 5 containers-storage.conf for more information
    # The "container storage" table contains all of the server options.
    [storage]

    # Default Storage Driver
    driver = "vfs"

    # Temporary storage location
    runroot = "/var/run/containers/storage"

    # Primary Read/Write location of container storage
    graphroot = "/var/lib/containers/storage"
    EOF

  - buildah run $buildcntr podman images || true

  # tag the current container
  - buildah commit --quiet $buildcntr $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
  # clean up the working container
  - buildah rm $buildcntr
   
  - podman images

  # bug when pushing 2 tags in the same repo with the same base,
  # this may fail. Just retry it after
  - podman push $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG || true
  - sleep 2
  - podman push $CI_REGISTRY_IMAGE/buildah:$BOOTSTRAP_TAG
